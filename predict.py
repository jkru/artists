from keras.models import load_model
import pickle
import numpy as np
import sys

def load(file_name):
    with open(file_name, 'rb') as f:
        return pickle.load(f)

def sample(preds, temperature=0.75):
    # helper function to sample an index from a probability array
    preds = np.asarray(preds).astype('float64')
    preds = np.log(preds) / temperature
    exp_preds = np.exp(preds)
    preds = exp_preds / np.sum(exp_preds)
    probas = np.random.multinomial(1, preds, 1)
    return np.argmax(probas)


def make_vec(name, char_to_indices, length):
    x = []
    for char in name:
        x.append([char_to_indices[char]])
    x = np.array(x) / float(length)
    return x

def main(text, temp):
    assert(len(text) == 1)
    char_to_indices = load('char_to_indices')
    esc_char_index = char_to_indices['<END>']
    indices_to_char = load('indices_to_char')
    model = load_model('model')
    length = len(char_to_indices)
    iteration = 0
    max_iteration = 50
    char = text
    word = text
    while True:
        x_pred = make_vec(char, char_to_indices, length)[0]
        rx = np.reshape(x_pred, ((x_pred.shape[0], 1, 1)))
        preds = model.predict(rx, verbose=0)[0]
        next_index = sample(preds, temp)
        next_char = indices_to_char[next_index]
        word += next_char
        print(next_char)
        char = next_char
        if next_index == esc_char_index:
            break
        iteration += 1
        if iteration == max_iteration:
            break
    print(word)


if __name__ == '__main__':
    text = sys.argv[1]
    temp = float(sys.argv[2])
    main(text, temp)
