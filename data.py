import sqlite3
import numpy as np
import json
from keras.preprocessing.sequence import pad_sequences
from keras.utils import np_utils
from collections import Counter
import unidecode
import re

regexp = re.compile(r"\(\d+\)$")

def load(file_name):
    with open(file_name, 'r') as f:
        return json.load(f)

def save(file_name, data):
    with open(file_name, 'w') as f:
        json.dump(data, f)

class DataProvider(object):
    def load(self, max_rows):
        conn = sqlite3.connect('artists.db')
        cur = conn.cursor()
        cur.execute('SELECT name FROM artists LIMIT {}'.format(max_rows))
        rows = cur.fetchall()
        result = []
        for row_number, row in enumerate(rows):
            if row_number == max_rows:
                break
            name = self._clean_name(row[0])
            result.append(name)
        return result

    def _clean_name(self, name):
        result = unidecode.unidecode(name.lower())
        result = re.sub(regexp, '', result)
        return result.strip()


class DataTransformer(object):
    def __init__(self, data, save_indices=True):
        self.data = data
        self.esc_char = '<END>'
        self.char_to_indices, self.indices_to_char = self._indices(self.data)
        self.esc_char_index = self.char_to_indices[self.esc_char]
        self.length = len(self.char_to_indices)
        if save_indices:
            save('char_to_indices.json', self.char_to_indices)
            save('indices_to_char.json', self.indices_to_char)

    def _indices(self, data):
        text = ''.join(data)
        chars = sorted(list(set(text))) + [self.esc_char]
        char_to_indices = {c: i for i, c in enumerate(chars)}
        indices_to_char = {i: c for i, c in enumerate(chars)}
        return char_to_indices, indices_to_char

    def _make_pair(self, name):
        x = []
        y = []
        for index, char in enumerate(name):
            x.append([self.char_to_indices[char]])
            if index < len(name) - 1:
                y.append(self.char_to_indices[name[index+1]])
            else:
                y.append(self.esc_char_index)
        x = np.array(x) / float(self.length)
        y = np_utils.to_categorical(y)
        return x, y

    def vectors(self):
        x = []
        y = []
        for name in self.data:
            _x, _y = self._make_pair(name)
            x.append(_x)
            y.append(_y)
        return {'x': x, 'y': y}
