from data import DataTransformer, DataProvider
from keras.callbacks import LambdaCallback
from keras.models import Model
from keras.layers import Dense, Activation, Input
from keras.layers import GRU, Embedding
from keras.optimizers import RMSprop
from keras.utils.data_utils import get_file
import numpy as np
from keras.callbacks import ModelCheckpoint
import sys
from time import gmtime, strftime
import tensorflow as tf
tf.enable_eager_execution()

class MyModel(tf.keras.Model):
  def __init__(self, vocab_size, embedding_dim, units, batch_size):
    super(MyModel, self).__init__()
    self.units = units
    self.batch_sz = batch_size
    self.embedding = Embedding(vocab_size, embedding_dim)
    self.gru = GRU(self.units, 
                   return_sequences=True, 
                   return_state=True, 
                   recurrent_activation='sigmoid', 
                   recurrent_initializer='glorot_uniform')

    self.fc = Dense(vocab_size)
        
  def call(self, x, hidden):
    x = self.embedding(x)

    # output shape == (batch_size, max_length, hidden_size) 
    # states shape == (batch_size, hidden_size)

    # states variable to preserve the state of the model
    # this will be used to pass at every step to the model while training
    output, states = self.gru(x, initial_state=hidden)


    # reshaping the output so that we can pass it to the Dense layer
    # after reshaping the shape is (batch_size * max_length, hidden_size)
    output = tf.reshape(output, (-1, output.shape[2]))

    # The dense layer will output predictions for every time_steps(max_length)
    # output shape after the dense layer == (max_length * batch_size, vocab_size)
    x = self.fc(output)

    return x, states

# def build_model(input_shape, y_size, vocab_size, embedding_dim):
#     inputs = Input(shape=input_shape)
#     x = Embedding(vocab_size, embedding_dim)(inputs)
#     x = GRU(1024, return_sequences=True, return_state=True, recurrent_initializer='glorot_uniform')(x)
#     predictions = Dense(y_size)(x)
#     model = Model(inputs=inputs, outputs=predictions)
#     model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
#     model.summary()
#     return model

def main(max_rows=1000000, epochs=60):
    data_provider = DataProvider()
    data = data_provider.load(max_rows=max_rows)
    data_transformer = DataTransformer(data=data)
    vectors = data_transformer.vectors()
    X = vectors['x']
    Y = vectors['y']
    # model = build_model(input_shape=(1,), 
    #                     y_size=data_transformer.length,
    #                     vocab_size=1,
    #                     embedding_dim=256)
    model = MyModel(
        vocab_size=data_transformer.length,
        embedding_dim=256,
        units=1024,
        batch_size=64
    )
    for i in range(epochs):
        time = strftime("%Y-%m-%d %H:%M:%S", gmtime())
        print('{} Epoch {}/{}'.format(time, i, epochs))
        for x, y in zip(X, Y):
            rx = np.reshape(x, ((x.shape[0], 1, 1)))
            model.fit(rx, y, 
                      epochs=1, 
                      batch_size=1,  
                      verbose=0,
                      shuffle=False)
        model.reset_states()
    model.save('model')


if __name__ == '__main__':
    print('args: max_rows, epochs')
    max_rows, epochs = int(sys.argv[1]), int(sys.argv[2])
    main(max_rows=max_rows, epochs=epochs)
