from word2vec import DataProvider
import markovify
import re

data_provider = DataProvider(max_rows=100000)
data = data_provider.load()
text = ' '.join(data)
reject_pat = re.compile(r"(^')|('$)|\s'|'\s|[\"(\(\)\[\])]")
text = re.sub(reject_pat, '', text)
print(text)

# Build the model.
text_model = markovify.Text(text)

# Print five randomly-generated sentences
# for i in range(5):
#     print(text_model.make_sentence())

# Print three randomly-generated sentences of no more than 140 characters
for i in range(100):
    print(i)
    print(text_model.make_short_sentence(15))
